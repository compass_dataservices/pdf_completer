#!python3.5

import csv
from fdfgen import forge_fdf
import os
import sys
import pdb
from tkinter import ttk, messagebox, filedialog

sys.path.insert(0, os.getcwd())
filename_prefix = "COMPASS FreedomPay Device Order Form_v3 01"
csv_file = "order_data_fidelity.csv"
pdf_file = "FP_dev_order.pdf"
tmp_file = "tmp.fdf"
output_folder = './out/'

file_type_filters = [('Supported Files', '.xls .xlsx .csv'),
                     ('CSV Files', '.csv'), ('PDF Files', '.pdf'),
                     ('Excel Files', '.xls .xlsx'), ('All Files', '.*')]

def open_file(options=None, filters=None):
    if not options:
        options = dict()
        options['filetypes'] = filters or file_type_filters
        options['title'] = 'Open...'
    file_opt = options
    
    file_selection = filedialog.askopenfilename(**file_opt)
    if not file_selection or file_selection == "":
        return
    else:
        return file_selection

def get_data():
    answer_file = open_file()
    if not answer_file or os.path.splitext(answer_file)[1] != '.csv':
        messagebox.showinfo(title='Oops',
                        message='This file is not supported.')
        return
    else:
        data = process_csv(answer_file)
        return data
        
def get_pdf_file():
    pdf_filters = [file_type_filters[-1]]
    for f in file_type_filters:
        if 'pdf' in f[1]:
            pdf_filters.insert(0, f)
    pdf_file = open_file(filters=pdf_filters)
    prefix = os.path.splitext(pdf_file)[0]
    return pdf_file, prefix
               
def process_csv(file):
    headers = []
    data =  []
    csv_data = csv.reader(open(file))
    for i, row in enumerate(csv_data):
      if i == 0:
        headers = row
        continue;
      field = []
      for i in range(len(headers)):
        field.append((headers[i], row[i]))
      data.append(field)
    return data

def form_fill(fields):
  fdf = forge_fdf("",fields,[],[],[])
  try:
    fdf_file = open(tmp_file,"w")
    fdf_file.write(fdf)
  except TypeError:
    fdf = fdf.decode('latin-1')
    fdf_file.write(fdf)
  fdf_file.close()
  output_file = '{0}{2}_{3}_{1}.pdf'.format(output_folder, filename_prefix, fields[6][1], fields[5][1])
  cmd = 'pdftk "{0}" fill_form "{1}" output "{2}" dont_ask'.format(pdf_file, tmp_file, output_file)
  print('Running command: {}'.format(cmd))
  os.system(cmd)
  os.remove(tmp_file)

pdf_file, filename_prefix = get_pdf_file()
data = get_data()
print('Generating Forms:')
print('-----------------------')
for i in data:
  if i[0][1] == 'Yes':
    continue
  print('{0} {1} created...'.format(filename_prefix, i[1][1]))
  form_fill(i)