# PDF Completer #

Allows for automatically filling out PDF Forms using standardized information from a database.  Currently used in [TPA Dashboard](https://bitbucket.org/compass_dataservices/project-dashboard) to handle FreedomPay Device Order and Switching Request Forms.